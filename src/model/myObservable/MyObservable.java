package model.myObservable;

import java.beans.PropertyChangeListener;

public interface MyObservable {

	void addPropertyChangeListener (PropertyChangeListener listener);
	
}
