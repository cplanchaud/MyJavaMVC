package model;

import contracts.IModel;
import model.myObservable.MyObservable;


public class Model implements IModel {
	
	private MyObservable subModel;
	
	public Model () {
		
	}
	
	public MyObservable getSubModel () {
		return this.subModel;
	}
	
	
}
