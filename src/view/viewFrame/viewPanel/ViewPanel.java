package view.viewFrame.viewPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JPanel;

import view.viewFrame.ViewFrame;

public class ViewPanel extends JPanel implements PropertyChangeListener, ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private ViewFrame frame;
	
	public ViewPanel(ViewFrame frame) {
		this.setFrame(frame);
		this.setLayout(null);
		
		
		
	}

	public void propertyChange(PropertyChangeEvent e) {
				
	}

	public void actionPerformed(ActionEvent arg0) {
		
	}
	
	public void setFrame (ViewFrame frame) {
		this.frame = frame;
	}
	
	

}
