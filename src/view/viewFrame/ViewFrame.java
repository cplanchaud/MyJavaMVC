package view.viewFrame;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JFrame;

import contracts.IController;
import view.viewFrame.viewPanel.ViewPanel;

public class ViewFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private IController controller;
	private ViewPanel panel;
	
	public ViewFrame(final String title, GraphicsConfiguration gc) throws HeadlessException {
		super(title, gc);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.panel = new ViewPanel(this);
	    this.setContentPane(this.panel);
		this.setSize(1800,1000);
		this.setLocationRelativeTo(null);
		
	}
	
	public void setController (final IController controller) {
		this.controller = controller;
	}
	
	public IController getController () {
		return this.controller;
	}
	
	public ViewPanel getPanel() {
		return this.panel;
	}

}
