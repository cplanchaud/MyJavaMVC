package view;

import javax.swing.SwingUtilities;

import contracts.IController;
import contracts.IView;
import view.viewFrame.ViewFrame;
import view.viewFrame.viewPanel.ViewPanel;

public class View implements IView, Runnable {
	
	private ViewFrame frame;
	
	public View () {
		this.frame = new ViewFrame("Arduino", null);
		SwingUtilities.invokeLater(this);
	}
	
	public void run() {
		this.frame.setVisible(true);
	}
	
	public void setController (final IController controller) {
		this.frame.setController(controller);
	}

	@Override
	public ViewPanel getPanel() {	
		return this.frame.getPanel();
	}

}
