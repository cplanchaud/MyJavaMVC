package contracts;

import model.myObservable.MyObservable;

public interface IModel {
	public MyObservable getSubModel();

}
