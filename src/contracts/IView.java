package contracts;

import view.viewFrame.viewPanel.ViewPanel;

public interface IView {
	public ViewPanel getPanel();
}
