package controller;

import contracts.IController;
import contracts.IModel;
import contracts.IView;

public class Controller implements IController {
	
	private IView view;
	private IModel model;
	
	public Controller (final IModel model, final IView view) {
		this.setModel(model);
		this.setView(view);
		this.getModel().getSubModel().addPropertyChangeListener(this.getView().getPanel());
		
	}
	
	private void setView (final IView view) {
		this.view = view;
	}
	
	public IView getView () {
		return this.view;
	}
	
	private void setModel (final IModel model) {
		this.model = model;
	}
	
	public IModel getModel () {
		return this.model;
	}

}
