package main;

import controller.Controller;
import model.Model;
import view.View;

public class Main {

	public static void main(String[] args) {
		
		final Model model = new Model();
		final View view = new View();
		final Controller controller = new Controller(model, view);
		view.setController(controller);

	}

}
